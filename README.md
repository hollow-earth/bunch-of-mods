# Bunch of Mods

The sequel to SECM

# Changelog
## 1.4.0
Added:
- Chunkloaders
- Re-chiseled
- Tesseract

## 1.3.0
Removed:
- Epic Fight
- Mystical Agriculture
- Mystical Aggraditions

## 1.2.0
Added:
- Polymorph

Config:
- PneumaticCraft: increased elevator height per elevator base from 6 to 12

## 1.1.0
Added:
- Croptopia
- Croptopia's Chocolaterie
- Farmer's Delights
- Nether's Delight
- The One Probe
- Ultimate Car Mod
- Ultimate Plane Mod
- Alchemistry
	- Bookshelf
	- GameStages

Removed:
 - IndustrialCraft2: clearly not updated and worth the time to deal with it.
 - Pam's HarvestCraft: annoying to deal with half the missing mods.
 - WTHIT: replaced with The One Probe

Config:
- The One Probe: available at all times

## 1.0.0
Test commit. Uploading the changelog
Added all the initial mods.

# Initial Modlist